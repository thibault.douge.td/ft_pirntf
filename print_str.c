/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_str.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tdouge <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/17 15:49:40 by tdouge            #+#    #+#             */
/*   Updated: 2017/01/17 15:49:41 by tdouge           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void	ft_print(char *str, t_ut *ut)
{
	int s;

	s = 0;
	while (str[ut->i] && s != 30)
	{
		if (str[ut->i] == '%')
			return ;
		ft_putchar(str[ut->i++]);
		ut->value_ret++;
		s++;
	}
}

void	ft_save_2(va_list ap, t_ut *ut)
{
	ut->to_conv += (ut->type == 208) ? va_arg(ap, long int) : 0;
	ut->to_conv += (ut->type == 316) ? va_arg(ap, long long int) : 0;
	ut->to_conv += (ut->type == 206) ? va_arg(ap, intmax_t) : 0;
	ut->to_conv_u += (ut->type == 222 || ut->type == 239 || ut->type == 233 ||
		ut->type == 242 || ut->type == 210) ? va_arg(ap, size_t) : 0;
	ut->to_conv_u += (ut->type == 117 || ut->type == 111 || ut->type == 120 ||
		ut->type == 88) ? va_arg(ap, unsigned int) : 0;
	ut->to_conv_u += (ut->type == 221 || ut->type == 215 || ut->type == 224 ||
		ut->type == 192) ? (unsigned short int)va_arg(ap, int) : 0;
	ut->to_conv_u += (ut->type == 325 || ut->type == 319 || ut->type == 328 ||
		ut->type == 296) ? (unsigned char)va_arg(ap, int) : 0;
	ut->to_conv_u += (ut->type == 225 || ut->type == 219 || ut->type == 228 ||
		ut->type == 196) ? va_arg(ap, unsigned long int) : 0;
	ut->to_conv_u += (ut->type == 333 || ut->type == 327 || ut->type == 336 ||
		ut->type == 304) ? va_arg(ap, unsigned long long int) : 0;
	ut->to_conv_u += (ut->type == 223 || ut->type == 217 || ut->type == 226 ||
		ut->type == 194) ? va_arg(ap, uintmax_t) : 0;
}

void	ft_save(va_list ap, t_ut *ut)
{
	if (ut->type == 'S')
		ft_chaine_uni(va_arg(ap, wchar_t *), ut);
	if (ut->type == 'C')
		ut->print_s = ft_regroup(va_arg(ap, wchar_t), ut);
	if (ut->type == 'c')
		ut->print_s = ft_add_one_to_str((char)va_arg(ap, int), ut);
	if (ut->type == 's')
		ut->print_s = ft_strdup(va_arg(ap, char *));
	if (ut->type == '%')
		ut->print_s = ft_strdup("%");
	ut->to_conv_u += (ut->type == 'p') ?
	(unsigned long int)va_arg(ap, void *) : 0;
	ut->to_conv += (ut->type == 100) ? va_arg(ap, int) : 0;
	ut->to_conv += (ut->type == 204) ? (short int)va_arg(ap, int) : 0;
	ut->to_conv += (ut->type == 308) ? (char)va_arg(ap, int) : 0;
	ft_save_2(ap, ut);
}

void	ft_type_2(t_ut *ut, char *str)
{
	int i;

	i = ut->i2;
	while (str[i] != 's' && str[i] != 'S' && str[i] != 'p' &&
		str[i] != 'd' && str[i] != 'i'
		&& str[i] != 'o' && str[i] != 'O' && str[i] != 'u' &&
		str[i] != 'U' && str[i] != 'D' && str[i] != 'x' && str[i] != 'X'
		&& str[i] != 'c' && str[i] != 'C' && str[i] != '%' && str[i])
		ut->type += str[i++];
	ut->type += str[i++];
	ut->type = (ut->type == 207) ? 'C' : ut->type;
	if (str[i - 1] < 86)
	{
		ut->type = (str[i - 1] == 'U') ? 225 : ut->type;
		ut->type = (str[i - 1] == 'O') ? 219 : ut->type;
		ut->type = (str[i - 1] == 'D') ? 208 : ut->type;
	}
	ut->type = (ut->type == 220) ? 'p' : ut->type;
	ut->type = (ut->type == 189) ? 225 : ut->type;
	ut->type = (ut->type == 275) ? 'C' : ut->type;
	ut->type = (ut->type == 291) ? 'S' : ut->type;
	ut->type = (ut->type == 223 && str[i - 1] == 's') ? 'S' : ut->type;
	if (str[i - 1] == 'i')
		ut->type = ut->type - 5;
	ut->i = i;
}

void	ft_type(t_ut *ut, char *str)
{
	int i;

	i = ut->i + 1;
	ut->type = 0;
	while (str[i] != 'h' && str[i] != 'l' && str[i] != 'L' && str[i] != 'j' &&
		str[i] != 'z' && str[i] != 's' && str[i] != 'S' &&
		str[i] != 'p' && str[i] != 'd' && str[i] != 'i'
		&& str[i] != 'D' && str[i] != 'o' && str[i] != 'O'
		&& str[i] != 'u' && str[i] != 'U' &&
		str[i] != 'x' && str[i] != 'X' && str[i] != 'c'
		&& str[i] != 'C' && str[i] != '%' && str[i])
		i++;
	ut->i2 = i;
	ft_type_2(ut, str);
}
