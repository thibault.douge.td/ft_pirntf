/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   chaine_uni.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tdouge <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/17 15:47:28 by tdouge            #+#    #+#             */
/*   Updated: 2017/01/18 14:28:48 by tdouge           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void	ft_chaine_uni(wchar_t *str, t_ut *ut)
{
	int		i;
	char	*tmp;

	i = 0;
	if (str == NULL)
	{
		ut->print_s = ft_strdup("(null)");
		return ;
	}
	while (str[i])
	{
		tmp = ut->print_s;
		ut->print_s = ft_strjoin_free(ut->print_s, ft_regroup(str[i], ut));
		free(tmp);
		i++;
	}
	if (str[0] == '\0')
		ut->print_s = ft_strnew(0);
}
