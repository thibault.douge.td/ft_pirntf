/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   verif_flag.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tdouge <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/17 15:49:53 by tdouge            #+#    #+#             */
/*   Updated: 2017/01/17 15:49:54 by tdouge           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int		ft_verif_zero(char *str)
{
	int i;

	i = 0;
	while (str[i])
	{
		if (str[i] == '0')
			return (1);
		if (str[i] > '0' && str[i] <= '9')
			return (0);
		i++;
	}
	return (0);
}

int		ft_verif_point(char *str)
{
	int i;

	i = 0;
	while (str[i])
	{
		if (str[i] == '.')
			return (0);
		if (ft_isdigit(str[i]) == 1)
			return (1);
		i++;
	}
	return (0);
}

void	ft_verif_diez(char *str, t_ut *ut)
{
	int i;

	i = 0;
	while (str[i])
	{
		if (str[i] == '#')
		{
			i = 0;
			while (ut->to_print[i])
			{
				if (ut->to_print[i] == 'x')
					ut->to_print[i] = '0';
				i++;
			}
			ut->to_print[1] = 'x';
			break ;
		}
		i++;
	}
}
