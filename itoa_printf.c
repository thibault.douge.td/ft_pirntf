/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   itoa_printf.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tdouge <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/17 15:48:31 by tdouge            #+#    #+#             */
/*   Updated: 2017/01/17 15:48:33 by tdouge           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void			ft_choice_itoa(t_ut *ut)
{
	if (ut->type == 111 || ut->type == 215 || ut->type == 319 ||
		ut->type == 219 || ut->type == 327 || ut->type == 217 ||
		ut->type == 233)
		ft_itoa_printf(ut, 8, 'a');
	else if (ut->type == 'p' || ut->type == 120 || ut->type == 224 ||
		ut->type == 328 || ut->type == 228 || ut->type == 336 ||
		ut->type == 226 || ut->type == 242)
		ft_itoa_printf(ut, 16, 'a');
	else if (ut->type == 88 || ut->type == 192 || ut->type == 296 ||
		ut->type == 196 || ut->type == 304 || ut->type == 194 ||
		ut->type == 210)
		ft_itoa_printf(ut, 16, 'A');
	else if (ut->type == 117 || ut->type == 221 || ut->type == 325 ||
		ut->type == 225 || ut->type == 333 || ut->type == 223 ||
		ut->type == 239)
		ft_itoa_printf(ut, 10, 'a');
	else if (ut->type == 100 || ut->type == 204 || ut->type == 308 ||
		ut->type == 208 || ut->type == 316 || ut->type == 206 ||
		ut->type == 222)
		ft_itoa_printf_n(ut);
	else if (ut->type != 's' && ut->type != 'c' && ut->type != 37
		&& ut->type != 'S' && ut->type != 'C')
		return ;
}

static int		ft_nb_malloc(t_ut *ut, unsigned long long int n, int i)
{
	int						count;
	unsigned long long int	tmp;

	count = 0;
	if (ut->type == 'p')
		count = 2;
	tmp = n;
	tmp = (tmp == 0 && ++count) ? tmp : tmp;
	while (tmp > 0)
	{
		tmp = tmp / i;
		count++;
	}
	return (count);
}

void			ft_itoa_printf(t_ut *ut, int i, char c)
{
	unsigned long long int	tmp;
	int						count;
	unsigned long long int	n;

	n = ut->to_conv_u;
	tmp = n;
	count = ft_nb_malloc(ut, n, i);
	if (n == 0 && ut->type != 'p')
	{
		ut->to_print = ft_strdup("0");
		return ;
	}
	if (!(ut->to_print = (char *)malloc(sizeof(*ut->to_print) * (count) + 1)))
		return ;
	ut->to_print[count] = '\0';
	ft_itoa_printf2(ut, i, c, count);
}

static void		ft_neg_itoa(t_ut *ut)
{
	long long int	tmp;
	int				count;
	long long int	n;

	n = ut->to_conv;
	if (n < -9223372036854775807)
	{
		ut->to_print = ft_strdup("-9223372036854775808");
		return ;
	}
	tmp = n * -1;
	count = 2;
	while (tmp / 10 > 0 && (tmp = tmp / 10))
		count++;
	tmp = n * -1;
	if (!(ut->to_print = (char *)malloc(sizeof(*ut->to_print) * (count) + 1)))
		return ;
	ut->to_print[count] = '\0';
	while (tmp > 0)
	{
		ut->to_print[--count] = (tmp % 10) + '0';
		tmp = tmp / 10;
	}
	ut->to_print[0] = '-';
	return ;
}

void			ft_itoa_printf_n(t_ut *ut)
{
	long long int		tmp;
	int					count;

	ut->to_conv = (ut->type == 222) ? ut->to_conv_u : ut->to_conv;
	tmp = ut->to_conv;
	count = 0;
	while (tmp > 0)
		tmp = (count++ || tmp > 0) ? (tmp / 10) : 0;
	if (ut->to_conv <= 0)
	{
		if (ut->to_conv < 0)
			ft_neg_itoa(ut);
		else
			ut->to_print = ft_strdup("0");
		return ;
	}
	if (!(ut->to_print = (char *)malloc(sizeof(*ut->to_print) * (count) + 1)))
		return ;
	ut->to_print[count] = '\0';
	tmp = ut->to_conv;
	while (tmp > 0)
	{
		ut->to_print[--count] = (tmp % 10) + '0';
		tmp = tmp / 10;
	}
}
