/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   begin.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tdouge <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/17 15:45:39 by tdouge            #+#    #+#             */
/*   Updated: 2017/01/17 15:47:14 by tdouge           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void	ft_begin(va_list ap, t_ut *ut, char *str)
{
	ut->to_conv = 0;
	ut->to_conv_u = 0;
	if (ut->to_print != NULL)
		free(ut->to_print);
	ut->to_print = NULL;
	if (ut->print_s != NULL)
		free(ut->print_s);
	ut->print_s = NULL;
	ft_type(ut, str);
	ft_save(ap, ut);
	ft_choice_itoa(ut);
	if (ut->type == 0)
	{
		ut->type = -42;
		return ;
	}
	if (ft_verif_flag(ut, str) == -1)
		return ;
}
