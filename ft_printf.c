/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tdouge <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/17 15:48:10 by tdouge            #+#    #+#             */
/*   Updated: 2017/01/17 15:48:13 by tdouge           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"
#include <limits.h>

t_ut	*ft_initialise_struct(void)
{
	t_ut *ut;

	ut = malloc(sizeof(t_ut));
	ut->value_ret = 0;
	ut->i = 0;
	ut->i2 = 0;
	ut->print_s = NULL;
	ut->to_print = NULL;
	return (ut);
}

void	ft_freestruct(t_ut *ut)
{
	free(ut->to_print);
	free(ut->print_s);
	free(ut);
}

void	ft_printf2(t_ut *ut)
{
	if (ut->print_s != NULL)
	{
		if (ut->check_nul == 1 && ut->check_neg == 1)
			write(1, "\0", 1);
		write(1, ut->print_s, ft_strlen(ut->print_s));
		ut->value_ret += ft_strlen(ut->print_s);
		if (ut->check_nul == 1 && ut->check_neg == 0)
			write(1, "\0", 1);
	}
	else if (ut->to_print != NULL)
	{
		write(1, ut->to_print, ft_strlen(ut->to_print));
		ut->value_ret += ft_strlen(ut->to_print);
	}
}

int		ft_printf(char *str, ...)
{
	t_ut	*ut;
	va_list	ap;
	int		tmp;

	va_start(ap, str);
	ut = ft_initialise_struct();
	while (str[ut->i] && ut->i2 != -1)
	{
		ft_print(str, ut);
		if (str[ut->i] == '%')
		{
			ft_begin(ap, ut, str);
			if (ut->type == -42)
			{
				ft_freestruct(ut);
				return (0);
			}
			ft_printf2(ut);
		}
	}
	va_end(ap);
	tmp = ut->value_ret;
	ft_freestruct(ut);
	return (tmp);
}
