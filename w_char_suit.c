/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   w_char_suit.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tdouge <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/18 14:59:43 by tdouge            #+#    #+#             */
/*   Updated: 2017/01/18 14:59:46 by tdouge           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

static int		ft_nb_malloc_base2(unsigned int n)
{
	int count;
	int tmp;

	tmp = n;
	count = 0;
	while (tmp > 0)
	{
		tmp = tmp / 2;
		count++;
	}
	return (count);
}

char			*ft_itoa_base2(unsigned int n)
{
	char	*str;
	int		tmp;
	int		count;

	tmp = n;
	count = ft_nb_malloc_base2(n);
	if (n == 0)
		return (ft_strdup("0"));
	if (!(str = (char *)malloc(sizeof(*str) * (count) + 1)))
		return (NULL);
	str[count] = '\0';
	while (tmp > 0)
	{
		str[--count] = (tmp % 2) + '0';
		tmp = tmp / 2;
	}
	return (str);
}

char			*ft_mask_2(char *tmp, char *str, int i, int j)
{
	while (i != -1 && j != -1)
	{
		while (str[j] == 'x' && i != -1 && j != -1)
		{
			str[j] = tmp[i];
			j--;
			i--;
		}
		j--;
	}
	i = -1;
	while (str[++i])
		if (str[i] == 'x')
			str[i] = '0';
	free(tmp);
	return (str);
}

char			*ft_mask(char *tmp)
{
	char	*str;
	int		i;
	int		j;

	str = NULL;
	i = ft_strlen(tmp) - 1;
	if (i < 7)
		str = tmp;
	if (i >= 7 && i <= 11)
		str = ft_strdup("110xxxxx10xxxxxx");
	if (i > 11 && i <= 16)
		str = ft_strdup("1110xxxx10xxxxxx10xxxxxx");
	if (i > 16)
		str = ft_strdup("11110xxx10xxxxxx10xxxxxx10xxxxxx");
	j = ft_strlen(str) - 1;
	str = ft_mask_2(tmp, str, i, j);
	return (str);
}
