/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   itoa_printf_suit.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tdouge <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/17 15:48:42 by tdouge            #+#    #+#             */
/*   Updated: 2017/01/17 15:48:43 by tdouge           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void	ft_itoa_printf2(t_ut *ut, int i, char c, int count)
{
	unsigned long long int tmp;
	unsigned long long int n;

	n = ut->to_conv_u;
	tmp = ut->to_conv_u;
	while (tmp > 0)
	{
		if (tmp % i < 10)
			ut->to_print[--count] = (tmp % i) + '0';
		else
			ut->to_print[--count] = (tmp % i) + c - 10;
		tmp = tmp / i;
	}
	if (count != 0)
	{
		count += ((ut->to_print[0] = '0') &&
			(ut->to_print[1] = 'x')) ? 0 : 0;
		count += (n == 0 && (ut->to_print[2] = '0')) ? 0 : 0;
	}
}
