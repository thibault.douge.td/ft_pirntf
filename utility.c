/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utility.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tdouge <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/17 15:49:45 by tdouge            #+#    #+#             */
/*   Updated: 2017/01/17 15:49:46 by tdouge           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

char	*ft_strjoin_free(char *s1, char *s2)
{
	char	*str;
	int		i;

	i = 0;
	if (s1 == NULL)
		return (s2);
	if (s1 && s2)
		i = ft_strlen(s1) + ft_strlen(s2);
	str = (char*)malloc(sizeof(*str) * (i + 1));
	if (!str)
		return (NULL);
	if (s1 && s2)
	{
		ft_strcpy(str, s1);
		ft_strcpy(str + ft_strlen(s1), s2);
	}
	free(s2);
	return (str);
}

char	*ft_strjoin_free_p(char *s1, char *s2, int p, char c)
{
	char	*str;
	int		i;

	i = 0;
	str = (char*)malloc(sizeof(char) * (p + 1));
	p -= ft_strlen(s2);
	if (!str)
		return (NULL);
	i = p;
	if (s1 && s2)
	{
		str[p] = '\0';
		while (p > 0)
		{
			p--;
			str[p] = c;
		}
		ft_strcpy(str + i, s2);
	}
	free(s2);
	return (str);
}

char	*ft_strjoin_free_end(char *s1, char *s2, int p, char c)
{
	char	*str;
	int		i;

	i = 0;
	str = (char*)malloc(sizeof(char) * (p + 1));
	if (!str)
		return (NULL);
	i = p;
	if (s1 && s2)
	{
		str = ft_strcpy(str, s2);
		str[p] = '\0';
		while (p > (int)ft_strlen(s2))
		{
			p--;
			str[p] = c;
		}
	}
	free(s2);
	return (str);
}

char	*ft_add_one_to_str(char c, t_ut *ut)
{
	char *str;

	ut->check_nul = 0;
	str = ft_strnew(1);
	str[0] = c;
	if (c == 0)
	{
		ut->value_ret++;
		ut->check_nul = 1;
	}
	return (str);
}

int		ft_isdigit_p(int c)
{
	if (c >= 49 && c <= 57)
		return (1);
	return (0);
}
