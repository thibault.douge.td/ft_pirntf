/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   w_char.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tdouge <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/17 15:49:57 by tdouge            #+#    #+#             */
/*   Updated: 2017/01/18 14:55:59 by tdouge           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

char			**ft_conv_wchar_2(char *str, int i, char **tab)
{
	int j;
	int t;

	j = 0;
	t = 0;
	while (i == 24 && j != 24)
	{
		if (tab == NULL)
			tab = (char **)malloc(sizeof(char*) * 4);
		tab[t++] = ft_strsub(str, j, 8);
		j += 8;
		tab[3] = 0;
	}
	while (i == 32 && j != 32)
	{
		if (tab == NULL)
			tab = (char **)malloc(sizeof(char*) * 5);
		tab[t++] = ft_strsub(str, j, 8);
		j += 8;
		tab[4] = 0;
	}
	free(str);
	return (tab);
}

char			**ft_conv_wchar(char *str)
{
	int		i;
	int		t;
	char	**tab;
	int		j;

	tab = NULL;
	t = 0;
	j = 0;
	i = ft_strlen(str);
	while (i == 16 && j != 16)
	{
		if (tab == NULL)
			tab = (char **)malloc(sizeof(char*) * 3);
		tab[t++] = ft_strsub(str, j, 8);
		j += 8;
		tab[2] = 0;
	}
	if (tab == NULL)
		tab = ft_conv_wchar_2(str, i, tab);
	else
		free(str);
	return (tab);
}

unsigned int	ft_conv_bin(char *str)
{
	int i;
	int nb;
	int j;

	nb = 0;
	j = 1;
	i = ft_strlen(str) - 1;
	while (i != -1)
	{
		if (str[i] == '1')
			nb += j;
		i--;
		j = j * 2;
	}
	return (nb);
}

int				*ft_tab(char **str)
{
	int i;
	int *tab;

	i = 0;
	while (str[i])
		i++;
	tab = (int *)malloc(sizeof(int) * i);
	i = 0;
	while (str[i])
	{
		tab[i] = ft_conv_bin(str[i]);
		i++;
	}
	i = 0;
	while (str[i])
		free(str[i++]);
	free(str);
	return (tab);
}

char			*ft_regroup(wchar_t a, t_ut *ut)
{
	char	*str;
	char	**tab;
	int		*tabo;
	int		i;

	i = 0;
	if (a < 128)
	{
		str = ft_add_one_to_str((char)a, ut);
		return (str);
	}
	str = ft_itoa_base2(a);
	str = ft_mask(str);
	tab = ft_conv_wchar(str);
	while (tab[i])
		i++;
	tabo = ft_tab(tab);
	str = (char *)malloc(sizeof(char) * (i) + 1);
	str[i] = '\0';
	while (--i != -1)
		str[i] = tabo[i];
	free(tabo);
	return (str);
}
