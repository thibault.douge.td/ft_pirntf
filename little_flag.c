/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   little_flag.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tdouge <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/17 15:49:16 by tdouge            #+#    #+#             */
/*   Updated: 2017/01/17 15:49:18 by tdouge           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int		ft_diez(t_ut *ut, char *str)
{
	if ((str[ut->i - 1] == 'o' || str[ut->i - 1] == 'O') &&
		ut->to_print[0] != '0')
		ut->to_print = ft_strjoin_free("0", ut->to_print);
	else if (str[ut->i - 1] == 'o' && ut->to_print[0] != '0' && ut->type == 111)
		ut->to_print = ft_strjoin_free("0", ut->to_print);
	else if (str[ut->i - 1] == 'x' && ut->to_conv_u > 0)
		ut->to_print = ft_strjoin_free("0x", ut->to_print);
	else if (str[ut->i - 1] == 'X' && ut->to_conv_u > 0)
		ut->to_print = ft_strjoin_free("0X", ut->to_print);
	return (1);
}

int		ft_plus(t_ut *ut, char *str)
{
	if (ut->to_print != NULL && ut->to_conv >= 0 && ut->to_conv_u == 0 &&
		(str[ut->i - 1] == 'd' || str[ut->i - 1] == 'D' ||
			str[ut->i - 1] == 'i'))
		ut->to_print = ft_strjoin_free("+", ut->to_print);
	return (1);
}

int		ft_space(t_ut *ut, char *str)
{
	if (ut->to_print != NULL && ut->to_conv >= 0 && ut->to_print[0] != '+'
		&& (str[ut->i - 1] == 'd' || str[ut->i - 1] == 'i'))
		ut->to_print = ft_strjoin_free(" ", ut->to_print);
	return (1);
}
