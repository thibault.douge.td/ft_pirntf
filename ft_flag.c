/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_flag.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tdouge <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/17 15:47:45 by tdouge            #+#    #+#             */
/*   Updated: 2017/01/17 15:47:49 by tdouge           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void	ft_initialise(t_ut *ut, int tmp)
{
	if (ut->i2 == ut->i && ut->check != 7)
	{
		ut->i2 = tmp;
		ut->check++;
	}
}

void	ft_verif_flag3(t_ut *ut, char *str, int check, int tmp)
{
	ut->i2 = (str[ut->i2] == '#' && ut->check == 4 &&
			ft_diez(ut, str) && ++ut->check) ? tmp : ut->i2;
	ut->i2 = (ft_isdigit_p(str[ut->i2]) && ut->check == 5 &&
			(ut->type != 115 && ut->type != 99 && ut->type != 37 &&
				ut->type != 67 && ut->type != 'S') &&
			ft_largeur(ut, str, check) && ++ut->check) ? tmp : ut->i2;
	ut->i2 = (ft_isdigit_p(str[ut->i2]) && ut->check == 6 &&
			(ut->type == 115 || ut->type == 99 || ut->type == 37 ||
				ut->type == 67 || ut->type == 'S') &&
			ft_largeur_s(ut, str, check) && ++ut->check) ? tmp : ++ut->i2;
	ft_initialise(ut, tmp);
}

int		ft_verif_flag2(t_ut *ut, char *str, int check)
{
	int tmp;

	ut->check_neg = 0;
	tmp = ut->i2;
	while (ut->i2 != ut->i && ut->check != 7)
	{
		ut->i2 = (str[ut->i2] == '-' && ut->check == 0 &&
			++ut->check && (check = 1) &&
			(ut->check_neg = 1)) ? tmp : ut->i2;
		ut->i2 = (str[ut->i2] == '.' && ut->check == 1 && ut->type != 37 &&
			ft_prez(ut, str) && ++ut->check && (check = 2 + check)) ?
		tmp : ut->i2;
		ut->i2 = (str[ut->i2] == '+' && ut->check == 2 &&
			ft_plus(ut, str) && ++ut->check) ? tmp : ut->i2;
		ut->i2 = (str[ut->i2] == ' ' && ut->check == 3 &&
			ft_space(ut, str) && ++ut->check) ? tmp : ut->i2;
		ft_verif_flag3(ut, str, check, tmp);
	}
	return (1);
}

int		ft_verif_flag(t_ut *ut, char *str)
{
	int check;

	check = 0;
	ut->check = 0;
	while (str[--ut->i2] != '%')
	{
		if ((str[ut->i2] < 48 || str[ut->i2] > 57) && str[ut->i2] != 43
				&& str[ut->i2] != 45 && str[ut->i2] != 46 &&
				str[ut->i2] != 35 && str[ut->i2] != 32 && str[ut->i2] != 37)
			return (0);
	}
	ft_verif_flag2(ut, str, check);
	return (0);
}
