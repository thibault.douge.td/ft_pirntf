/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   largeur_s.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tdouge <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/17 15:49:05 by tdouge            #+#    #+#             */
/*   Updated: 2017/01/17 15:49:07 by tdouge           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int		ft_verif_zero_s(char *str)
{
	int i;

	i = 0;
	while (str[i])
	{
		if (str[i] == '0')
			return (1);
		if (str[i] > '0' && str[i] <= '9')
			return (0);
		i++;
	}
	return (0);
}

int		ft_verif_point_s(char *str)
{
	int i;

	i = 0;
	while (str[i])
	{
		if (str[i] == '.')
			return (0);
		if (ft_isdigit(str[i]) == 1)
			return (1);
		i++;
	}
	return (0);
}

void	ft_verif_diez_s(char *str, t_ut *ut)
{
	int i;

	i = 0;
	while (str[i])
	{
		if (str[i] == '#')
		{
			i = 0;
			while (ut->print_s[i])
			{
				if (ut->print_s[i] == 'x')
					ut->print_s[i] = '0';
				i++;
			}
			ut->print_s[1] = 'x';
			break ;
		}
		i++;
	}
}

int		ft_largeur_s(t_ut *ut, char *str, int check)
{
	int i;
	int tmp;

	if (ft_verif_point_s(str) == 0)
		return (0);
	if (str[0] == '0')
		return (0);
	i = ft_atoi(str + ut->i2);
	tmp = ft_strlen(ut->print_s);
	if (ut->print_s[0] == 0 && ut->type == 'c')
		i--;
	if (i > tmp)
	{
		if (check == 1 || check == 3)
			ut->print_s = ft_strjoin_free_end(" ", ut->print_s, i, ' ');
		else
			ut->print_s = ft_strjoin_free_p(" ", ut->print_s, i, ' ');
	}
	if (ft_verif_zero_s(str) == 1 && (check < 1 || check == 2) && (i = -1))
	{
		while (ut->print_s[++i])
			ut->print_s[i] = (ut->print_s[i] == ' ') ? '0' : ut->print_s[i];
		ft_verif_diez_s(str, ut);
	}
	return (1);
}
