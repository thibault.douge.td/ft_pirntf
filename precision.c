/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   precision.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tdouge <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/17 15:49:32 by tdouge            #+#    #+#             */
/*   Updated: 2017/01/17 15:49:34 by tdouge           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int		ft_prez_s(t_ut *ut, char *str)
{
	int i;
	int tmp;

	if (ut->type == 99)
		return (1);
	i = ft_atoi(&str[ut->i2 + 1]);
	tmp = ft_strlen(ut->print_s);
	if (ut->to_conv < 0)
		i++;
	if (tmp > i)
		ut->print_s[i] = '\0';
	return (1);
}

int		ft_prez_3(t_ut *ut, int i, int tmp)
{
	if (ut->to_conv < 0)
		i++;
	if (tmp < i)
		ut->to_print = ft_strjoin_free_p("0", ut->to_print, i, '0');
	if (ut->to_conv < 0 && tmp < i)
	{
		i = 0;
		while (ut->to_print[i])
			ut->to_print[i] = (ut->to_print[i] == '-' &&
				(ut->to_print[0] = '-')) ? '0' : ut->to_print[++i];
	}
	tmp = -1;
	while (ut->type == 'p' && i > 0)
		if (ut->to_print[++tmp] == 'x')
		{
			ut->to_print[tmp] = '0';
			ut->to_print[1] = 'x';
			return (1);
		}
	return (1);
}

int		ft_prez_2(t_ut *ut, char *str)
{
	int i;
	int tmp;

	i = ft_atoi(&str[ut->i2 + 1]);
	tmp = ft_strlen(ut->to_print);
	ut->type = (ut->type == 'p' && i > 0 && (i += 2)) ? ut->type : ut->type;
	if (i == 0 && ut->to_conv == 0 && ut->to_conv_u == 0)
	{
		ut->to_print[0] = '\0';
		ut->type = (ut->type == 'p' &&
			(ut->to_print[0] = '0') &&
			(ut->to_print[2] = 0)) ? ut->type : ut->type;
		return (1);
	}
	ft_prez_3(ut, i, tmp);
	return (1);
}

int		ft_prez(t_ut *ut, char *str)
{
	if (ut->type == 115 || ut->type == 99 || ut->type == 83 || ut->type == 67)
	{
		ft_prez_s(ut, str);
		return (1);
	}
	else
		ft_prez_2(ut, str);
	return (1);
}
