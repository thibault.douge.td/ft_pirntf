/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tdouge <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/17 15:48:20 by tdouge            #+#    #+#             */
/*   Updated: 2017/01/17 15:48:23 by tdouge           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_PRINTF_H
# define FT_PRINTF_H

# include "libft/libft.h"
# include "stdarg.h"
# include "stdio.h"

typedef struct	s_utils
{
	unsigned long long int	to_conv_u;
	long long int			to_conv;
	int						check;
	int						largeur;
	int						precision;
	int						value_ret;
	int						type;
	int						i;
	int						i2;
	char					*to_print;
	char					*print_s;
	int						check_nul;
	int						check_neg;
}				t_ut;

int				ft_verif_flag(t_ut *ut, char *str);
void			ft_print(char *str, t_ut *ut);
void			ft_begin(va_list ap, t_ut *ut, char *str);
void			ft_itoa_printf(t_ut *ut, int i, char c);
void			ft_itoa_hexa(t_ut *ut, char c);
void			ft_choice_itoa(t_ut *ut);
void			ft_itoa_printf_n(t_ut *ut);
char			*ft_add_one_to_str(char c, t_ut *ut);
int				ft_prez(t_ut *ut, char *str);
int				ft_largeur(t_ut *ut, char *str, int check);
char			*ft_strjoin_free_p(char *s1, char *s2, int p, char c);
char			*ft_strjoin_free_end(char *s1, char *s2, int p, char c);
int				ft_printf(char *str, ...);
int				ft_largeur_s(t_ut *ut, char *str, int check);
int				ft_isdigit_p(int c);
void			ft_begin(va_list ap, t_ut *ut, char *str);
void			ft_type(t_ut *ut, char *str);
void			ft_save(va_list ap, t_ut *ut);
void			ft_itoa_printf2(t_ut *ut, int i, char c, int count);
int				ft_verif_zero(char *str);
int				ft_verif_point(char *str);
void			ft_verif_diez(char *str, t_ut *ut);
int				ft_diez(t_ut *ut, char *str);
int				ft_plus(t_ut *ut, char *str);
int				ft_space(t_ut *ut, char *str);
char			*ft_strjoin_free(char *s1, char *s2);
char			*ft_regroup(wchar_t a, t_ut *ut);
void			ft_chaine_uni(wchar_t *str, t_ut *ut);
char			*ft_mask(char *tmp);
char			*ft_itoa_base2(unsigned int n);

#endif
