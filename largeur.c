/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   largeur.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tdouge <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/17 15:48:53 by tdouge            #+#    #+#             */
/*   Updated: 2017/01/17 15:48:55 by tdouge           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int		ft_largeur_3(t_ut *ut, char *str, int check, int space)
{
	int i;

	if (ft_verif_zero(str) == 1 && (check < 1 || check > 3))
	{
		i = 0;
		while (ut->to_print[i])
		{
			if (ut->to_print[i] == '+')
			{
				ut->to_print[i] = '0';
				ut->to_print[0] = '+';
			}
			i++;
		}
	}
	if (space == 1)
		ut->to_print[0] = ' ';
	return (1);
}

int		ft_largeur_2(t_ut *ut, char *str, int check, int space)
{
	int i;

	i = 0;
	if (ft_verif_zero(str) == 1 && (check < 1 || check > 3))
	{
		i = -1;
		while (ut->to_print[++i])
			ut->to_print[i] = (ut->to_print[i] == ' ') ? '0' : ut->to_print[i];
		i = -1;
		ft_verif_diez(str, ut);
		i = 0;
		while (ut->to_print[i])
		{
			if (ut->to_print[i] == '-')
			{
				ut->to_print[i] = '0';
				ut->to_print[0] = '-';
			}
			i++;
		}
	}
	ft_largeur_3(ut, str, check, space);
	return (1);
}

int		ft_largeur(t_ut *ut, char *str, int check)
{
	int i;
	int tmp;
	int space;

	space = (ut->to_print[0] == ' ') ? 1 : 0;
	if (ft_verif_point(str) == 0)
		return (0);
	if (str[0] == '0')
		return (0);
	i = ft_atoi(str + ut->i2);
	tmp = ft_strlen(ut->to_print);
	if (i > tmp)
	{
		i = i - tmp + tmp;
		if (check == 1 || check == 3)
			ut->to_print = ft_strjoin_free_end(" ", ut->to_print, i, ' ');
		else
			ut->to_print = ft_strjoin_free_p(" ", ut->to_print, i, ' ');
	}
	ft_largeur_2(ut, str, check, space);
	return (1);
}
