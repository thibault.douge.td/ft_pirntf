# ft_printf

Résumé: Le but de ce projet est de comprendre l'enjeu des fonctions variadiques, en recodant la fonction printf de la librairie <stdio.h>,
en y incluant un certain nombre d'options d'affichage.

### Langage autorisé:

- C

### Fonctions autorisées :

- malloc
- free
- write
- exit
- les fonctions du man 3 stdarg
